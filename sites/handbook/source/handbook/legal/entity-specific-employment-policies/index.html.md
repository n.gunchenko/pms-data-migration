---
layout: handbook-page-toc
title: "Entity-Specific Employment Policies"
description: "A directory of employment-related policies categorized by entity"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This page will serve as a directory for our team members employed through a GitLab entity to locate employment-related policies specific to their country of residence. These policies do not supersede [GitLab Inc.'s Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/). 

## GitLab Entities
<details>
  <summary markdown="span">GitLab BV (Belgium)</summary>
</details>
<details>
  <summary markdown="span">GitLab BV (Netherlands)</summary>
</details>
<details>
  <summary markdown="span">GitLab Canada Corp.</summary>
</details>
<details>
  <summary markdown="span">GitLab France S.A.S.</summary>
</details>
<details>
  <summary markdown="span">GitLab GmbH (Germany)</summary>

### Germany 
**Health and Safety**

To ensure the health and safety of our team members in Germany, and to maintain compliance with the German Occupational Safety and Health Act, all team members in Germany will complete the following checklist at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

- [Work from Home Checklist](https://docs.google.com/document/d/1Z3i-vrkcU5ald0j-scf2rgTumwFCc_sI/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)
</details>
<details>
  <summary markdown="span">GitLab GK (Japan)</summary>
</details>
<details>
  <summary markdown="span">GitLab Inc. (US)</summary>
</details>
<details>
  <summary markdown="span">GitLab Ireland LTD</summary>
</details>
<details>
  <summary markdown="span">GitLab Korea LTD</summary>
</details>
<details>
  <summary markdown="span">GitLab LTD (UK)</summary>
</details>
<details>
  <summary markdown="span">GitLab PTY (Australia and New Zealand)</summary>

### Australia
**Health and Safety**

To ensure the physical and mental health and safety of our team members in Australia, and to maintain compliance with local employment regulations, all team members in Australia will complete the following checklist at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

- [Remote Work Checklist](https://docs.google.com/document/d/1_sHk3ksGLDVxBZsnO3pMD-U_R_Fy0Yyu/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

### New Zealand
**Health and Safety**

To ensure the physical and mental health and safety of our team members in New Zealand, and to maintain compliance with local employment regulations, all team members in Australia will complete the following survey at onboarding. The responses will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

- [Remote Work Checklist](https://forms.gle/DszZNkBA22Rhy3VW7)
</details>
<details>
  <summary markdown="span">GitLab Singapore PTE LTD</summary>

**Health and Safety**

- [How to Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace)
- [Focus your Workspace](https://about.gitlab.com/company/culture/all-remote/getting-started/#focus-your-workspace)
- [Combating Burnout, Isolation, and Anxiety in a Remote Workplace](https://about.gitlab.com/company/culture/all-remote/mental-health/)
- [Considerations for a Productive Home Office](https://about.gitlab.com/company/culture/all-remote/workspace/#introduction)
- [Equipment Examples](https://about.gitlab.com/handbook/finance/expenses/#-not-sure-what-to-buy-equipment-examples)
- [Hardware Expense Guide](https://about.gitlab.com/handbook/finance/expenses/#-hardware)
 
**Data Protection/Privacy Policy**
- [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/)
- [GitLab Privacy Policy](https://about.gitlab.com/privacy/)
 
**Workplace Harassment Policy**
- [Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/#introduction)
- [Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
 
**Fair Employment Practices Policy**
- [GitLab PTE LTD Fair Employment Practices Policy](https://docs.google.com/document/d/1osJfO9BysOqjpt5iLnqygXX7B5Imb9NXIXA3KBtDCJk/edit?usp=sharing)
</details> 
